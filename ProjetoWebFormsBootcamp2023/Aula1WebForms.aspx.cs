﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjetoWebFormsBootcamp2023
{
    public partial class Aula1WebForms : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // vamos alimentar a dropdownlist com um listItem
            // ( id da dropdownlist).Itens.Add (new ListItem( 'info1', 'info2'))
            //if(!IsPostBack)
            //{
            //    ddlEstados.Items.Add(new ListItem("", "0"));
            //    ddlEstados.Items.Add(new ListItem("Amazonas", "1"));
            //    ddlEstados.Items.Add(new ListItem("Paraná", "2"));
            //    ddlEstados.Items.Add(new ListItem("Rio de Janeiro", "3"));
            //    ddlEstados.Items.Add(new ListItem("Pernambuco", "4"));
            //}           

            // vamos alimentar a dropdownlist com um datatable
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                // adicionando columas no datatable
                dt.Columns.Add("Estado", typeof(string));
                dt.Columns.Add("Valor", typeof(int));
                // adicionar as linhas do datatable
                dt.Rows.Add("", "0");
                dt.Rows.Add("Distrito Federal", "1");
                dt.Rows.Add("Rio Grande do Sul", "2");
                dt.Rows.Add("Minas Gerais", "3");
                //vamos fazer a vinculação do datatable com a dropdown
                //(id da dropdownlist).DataSource = nomedafontededados
                //(id da dropdownlist).datatextfield = (fontededados).columns["coluna para o usuario"].tostring();
                //(id da dropdownlist).datavaluefield = (fontededados).columns["coluna para o valor"].tostring();
                //(id da dropdownlist).databind; ( aqui é a vinculação dos dados com a dropdownlist)

                //if(!IsPostBack)
                //{
                //    ddlEstados.DataSource = dt;
                //    ddlEstados.DataTextField = dt.Columns["Estado"].ToString();
                //    ddlEstados.DataValueField = dt.Columns["Valor"].ToString();
                //    ddlEstados.DataBind();
                //}

                //Vamos alimentar a dropdownlist com uma lista
                //List<string> (Nome da lista) = new List<string>;
                //(Nome da lista).Add("o elemento da lista")
                List<string> lstEstados = new List<string>();
                lstEstados.Add("--Selecione o Estado--");
                lstEstados.Add("Ceará");
                lstEstados.Add("Bahia");
                lstEstados.Add("Sergipe");

                //(Nome da lista).Insert(índice,"o elemento da lista")
                List<string> lst2Estados = new List<string>();
                lst2Estados.Insert(0, "--Selecione o Estado--");
                lst2Estados.Insert(1, "Santa Catarina");
                lst2Estados.Insert(2, "Pernambuco");
                lst2Estados.Insert(3, "Espírito Santo");

                var listaEstados = lstEstados.Concat(lst2Estados);

                //listaEstados = listaEstados.;


                //(ID da DropDownsList).DataSource = nome da fonte de dados
                //(ID da DropDownsList).DataBind; ( aqui é feito a vinculação da fonte de dados com a DropDownList)
                ddlEstados.DataSource = listaEstados;
                ddlEstados.DataBind();
            }
            

            //if (ddlEstados.SelectedItem.Value != null)
            //{
                //lblDdlResultadoNaTela.Text = " O Texto selecionado na DropDownList é :" + ddlEstados.SelectedItem.Text;
            //}
        }

        protected void btnQuantidade_Click(object sender, EventArgs e)
        {
            //(ID da label).Text = "Frase para exibir na tela" + (ID da ListBox).Itens.Count.ToString();
            //lblResultadoNaTela.Text = "A quantidade de elementos na ListBox é " + LbxNomes.Items.Count.ToString();
        }

        protected void btnTextoSelecionado_Click(object sender, EventArgs e)
        {
            //(ID da label).Text = "Frase para exibir na tela" + (ID da ListBox).Itens.Count.ToString();
            //lblResultadoNaTela.Text = "O(s) Texto(s) selecionado(s) na ListBox é (são) " + LbxNomes.SelectedItem.Text.ToString();

            //Análise para adicionar na tela nos itens selecionados
            foreach (ListItem item in LbxNomes.Items)
            {
               
                    Response.Write(" O Texto selecionado é : " + item.Selected);
                
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            //Response.Write(ID_do_Textbox.Text);
            //Response.Write(TxtbNome.Text);
        }

        protected void btnDdlTextoSelecionado_Click(object sender, EventArgs e)
        {
            //(ID da label).Text = "Frase para exibir na tela" + (ID da ListBox).Itens.Count.ToString();
            //if(ddlEstados.SelectedItem != null)
            //{
                lblDdlResultadoNaTela.Text = " O Texto selecionado na DropDownList é :  " + ddlEstados.Text;
            //}

        }
        protected void ddlOpcoes_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Atualize o texto da Label com o valor selecionado no DropDownList
            lblSelecao.Text = "Opção selecionada: " + ddlOpcoes.SelectedItem.Text;
        }

        protected void chkCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(chkCheckBox.Checked)
            {
                lblChkResultadoNaTela.Text = "AVISO : Você selecionou o checkbox";
            }
            else
            {
                lblChkResultadoNaTela.Text = "AVISO : Você precisa selecionar o checkbox";
            }
        }

        protected void btnCdbLinguagens_Click(object sender, EventArgs e)
        {
            //1a forma com o FOREACH
            //lblClbResultadoNaTela.Text = "As linguagens preferidas selecionadas são : <br />";
            //foreach (ListItem item in CheckBoxList1.Items)
            //{
            //    if (item.Selected)
            //    {
            //        lblClbResultadoNaTela.Text += item.Text + "<br />";
            //    }

            //}

            //2a forma
            lblClbResultadoNaTela.Text = "As linguagens preferidas selecionadas são : <br />";
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    lblClbResultadoNaTela.Text += CheckBoxList1.Items[i].Text + "<br />";
                }
            }
        }

        protected void btbTipoFornecedor_Click(object sender, EventArgs e)
        {
            if (radbPessoaFisica.Checked)
            {
                lblBtnResultadoNaTela.Text = "AVISO : Você selecionou o fornecedor " + radbPessoaFisica.Text;
            }
            else
            {
                lblBtnResultadoNaTela.Text = "AVISO : Você selecionou o fornecedor " + radbPessoaJuridica.Text;
            }
        }
    }
}