﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Aula1WebForms.aspx.cs" Inherits="ProjetoWebFormsBootcamp2023.Aula1WebForms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Teste Ibid</title>
</head>
<body>
    <script language="javascript">
        function Exibir()
        {
            var nome = document.getElementById("TxtbNome").value;
            alert(nome);
        }
    </script>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Nome"></asp:Label>
        <asp:TextBox ID="TxtbNome" runat="server"></asp:TextBox>
        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" BackColor="#33CC33" Font-Bold="true" ForeColor="Black" OnClientClick="Exibir()"/>
        <br />
        <asp:ListBox ID="LbxNomes" runat="server" Height="96px" SelectionMode="Multiple">
            <asp:ListItem Value="1">Rodrigo</asp:ListItem>
            <asp:ListItem Value="2">Mauro</asp:ListItem>
            <asp:ListItem Value="3">Leanderson</asp:ListItem>
            <asp:ListItem Value="4">Diogo</asp:ListItem>
            <asp:ListItem Value="5">Milena</asp:ListItem>
        </asp:ListBox>
        <br />
        <asp:Button ID="btnQuantidade" runat="server" Text="Quantidade" OnClick="btnQuantidade_Click"/>
        <asp:Button ID="btnTextoSelecionado" runat="server" Text="Texto(s) Selecionado(s)" OnClick="btnTextoSelecionado_Click"/>
        <br />
        <asp:Label ID="lblResultadoNaTela" runat="server" Text=""></asp:Label>
        <br />
        <asp:DropDownList ID="ddlEstados" runat="server" AutoPostBack="True" OnSelectedIndexChanged="btnDdlTextoSelecionado_Click" ToolTip="Selecione uma opção !">
            <%--<asp:ListItem></asp:ListItem>
            <asp:ListItem Value="1">Amazonas</asp:ListItem>
            <asp:ListItem Value="2">Bahia</asp:ListItem>
            <asp:ListItem Value="3">Pernambuco</asp:ListItem>
            <asp:ListItem Value="4">Piauí</asp:ListItem>
            <asp:ListItem Value="5">Paraná</asp:ListItem>--%>
        </asp:DropDownList>
        <br />
        <asp:Button ID="btnDdlTextoSelecionado" runat="server" Text="Texto Selecionado" OnClick="btnDdlTextoSelecionado_Click"/>
        <br />
        <asp:Label ID="lblDdlResultadoNaTela" runat="server" Text=""></asp:Label>
        <br />
        <asp:DropDownList ID="ddlOpcoes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOpcoes_SelectedIndexChanged">
            <asp:ListItem Text="Opção 1" Value="1" />
            <asp:ListItem Text="Opção 2" Value="2" />
            <asp:ListItem Text="Opção 3" Value="3" />
        </asp:DropDownList>
        <br />
        <asp:Label ID="lblSelecao" runat="server" Text=""></asp:Label>
        <br />
        <asp:CheckBox ID="chkCheckBox" runat="server" OnCheckedChanged="chkCheckBox_CheckedChanged"/>
        <br />
        <asp:Button ID="btnChkVerificacao" runat="server" Text="Verificação" OnClick="btnDdlTextoSelecionado_Click"/>
        <br />
        <asp:Label ID="lblChkResultadoNaTela" runat="server" Text=""></asp:Label>
        <br />
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="4">
            <asp:ListItem Value="1">C#</asp:ListItem>
            <asp:ListItem Value="2">Java</asp:ListItem>
            <asp:ListItem Value="3">Fortran</asp:ListItem>
            <asp:ListItem Value="4">Matlab</asp:ListItem>
            <asp:ListItem Value="5">JavaScript</asp:ListItem>
            <asp:ListItem Value="6">Assembly</asp:ListItem>
            <asp:ListItem Value="7">Pascal</asp:ListItem>
            <asp:ListItem Value="8">Cobol</asp:ListItem>
        </asp:CheckBoxList>
        <br />
        <asp:Button ID="btnCdbLinguagens" runat="server" Text="Escolher linguagens preferidas" OnClick="btnCdbLinguagens_Click"/>
        <br />
        <asp:Label ID="lblClbResultadoNaTela" runat="server" Text=""></asp:Label>
        <br />
        <asp:RadioButton ID="radbPessoaFisica" runat="server" GroupName="GrpFornecedor" Text="Pessoa Física"/>
        <asp:RadioButton ID="radbPessoaJuridica" runat="server" GroupName="GrpFornecedor" Text="Pessoa Juridica"/>
        <br />
        <asp:Button ID="btbTipoFornecedor" runat="server" Text="Tipo Fornecedor" OnClick="btbTipoFornecedor_Click"/>
        <br />
        <asp:Label ID="lblBtnResultadoNaTela" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <br />
    </form>
</body>
</html>
